(function($){
	$('[data-toggle="datepicker"]').datepicker();

	$(".list-load").slice(0, 4).show();
    $(".list-load2").slice(0, 4).show();
    $(".list-load3").slice(0, 4).show();
    $(".list-load4").slice(0, 4).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".list-load:hidden").slice(0, 4).slideDown();
        if ($(".list-load:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
    $("#loadMore2").on('click', function (e) {
        e.preventDefault();
        $(".list-load2:hidden").slice(0, 4).slideDown();
        if ($(".list-load2:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
     $("#loadMore3").on('click', function (e) {
        e.preventDefault();
        $(".list-load3:hidden").slice(0, 4).slideDown();
        if ($(".list-load3:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
     $("#loadMore4").on('click', function (e) {
        e.preventDefault();
        $(".list-load4:hidden").slice(0, 4).slideDown();
        if ($(".list-load4:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
    $('.modal').modal('show');
})(jQuery);